package com.company.entity;

import com.company.Interfaces.Service;

import java.util.ArrayList;
import java.util.List;

public final class ServiceCache {
    final List<Service> services = new ArrayList<>();


    public Service getService (String serviceName){
        for (Service service: services){
            if(service.getName().equalsIgnoreCase(serviceName)){
                return service;
            }
        }
        return null;
    }

    public void addService(Service newService){
        boolean exist = false;
        for (Service service: services){
            if(service.getName().equalsIgnoreCase(newService.getName())){
                exist = true;
            }
        }
        if(!exist){
            services.add(newService);
        }
    }

}
