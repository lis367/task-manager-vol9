package com.company.entity;

import org.jetbrains.annotations.NotNull;
import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

import java.util.Date;

@Getter
@Setter

public final class Project {
    @NotNull
    private String name;
    @NotNull
    final String id;
    @Nullable
    private String description;
    @NotNull
    private Date dateBegin;
    @NotNull
    private Date dateEnd;
    @NotNull
    final String userId;
    @NotNull
    private Status displayName;
    @NotNull
    private Date creationDate;


    public Project(String name, Date start, Date end, String id, String userId) {
        this.name = name;
        this.dateBegin = start;
        this.dateEnd = end;
        this.id = id;
        this.userId = userId;
    }

}
