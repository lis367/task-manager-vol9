package com.company.entity;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Date;
@Getter
@Setter

public final class Task {
    @NotNull
    private String name;
    @NotNull
    final String id;
    @Nullable
    private String description;
    @NotNull
    private Date dateBegin;
    @NotNull
    private Date dateEnd;
    @NotNull
    private String projectID;
    @NotNull
    final String userId;
    @NotNull
    private Status displayName;
    @NotNull
    private Date creationDate;


    public Task(String name, String id, String userId) {
        this.name = name;
        this.id = id;
        this.userId = userId;
    }

}
