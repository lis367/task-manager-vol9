package com.company.entity;



import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;

@Setter
@Getter

public final class User {
    @NotNull
    private String name;
    @NotNull
    private String password;
    @NotNull
    private UserRoleType userRoleType;
    @NotNull
    private String userId;

    public User (){

    }

    public User(String name, String password) {
        this.name = name;
        this.password = password;
    }


}
