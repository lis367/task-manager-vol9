package com.company.commands.projectCommands;

import com.company.Interfaces.ProjectServiceInterface;
import com.company.Interfaces.ServiceLocator;
import com.company.commands.AbstractCommand;
import com.company.entity.Project;
import com.company.entity.Status;
import com.company.exception.EmptyField;
import com.company.exception.NoPermission;
import com.company.exception.ObjectIsNotFound;
import com.company.exception.WrongDateFormat;
import com.company.service.TerminalServiceImpl;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;


import java.text.SimpleDateFormat;
import java.util.Scanner;
@NoArgsConstructor

public class ProjectUpdateCommand extends AbstractCommand {

    @Override
    public String command() {
        return "project-update";
    }

    @Override
    public String description() {
        return "Update project";
    }

    @Override
    public void execute() throws Exception {
        @NotNull
        final ProjectServiceInterface projectService = (ProjectServiceInterface) serviceLocator.getService("Project-Service");
        final TerminalServiceImpl terminalService = (TerminalServiceImpl) serviceLocator.getService("Terminal-service");
        System.out.println("ENTER ID");
        String line = terminalService.nextLine();
        try {
            if (projectService.read(line).getUserId().equals(serviceLocator.getUser().getUserId())) {
                final Project projectPool = projectService.read(line);
                System.out.println("ENTER NEW NAME");
                line = terminalService.nextLine();
                if(line.isEmpty()){
                    throw new EmptyField();
                }
                projectPool.setName(line);
                System.out.println("ENTER THE TIME OF THE BEGINNING OF THE PROJECT");
                final String dateStart = terminalService.nextLine().trim();
                System.out.println("ENTER THE END TIME OF THE PROJECT");
                final String dateEnd = terminalService.nextLine().trim();
                if(!serviceLocator.isValidDate(dateStart)|!serviceLocator.isValidDate(dateEnd)){
                    throw new WrongDateFormat();
                }
                SimpleDateFormat dateFormatter = new SimpleDateFormat("dd.MM.yyyy");
                projectPool.setDateBegin(dateFormatter.parse(dateStart));
                projectPool.setDateEnd(dateFormatter.parse(dateEnd));
                System.out.println("ENTER THE STATUS OF THE PROJECT");
                System.out.println("1 - Pending, 2 - Running, 3 - Finished");
                line = terminalService.nextLine();
                if(line.isEmpty()){
                    throw new EmptyField();
                }
                switch (line){
                    case "1":
                        projectPool.setDisplayName(Status.PENDING);
                        break;
                    case "2":
                        projectPool.setDisplayName(Status.RUNNING);
                        break;
                    case "3":
                        projectPool.setDisplayName(Status.FINISHED);
                        break;
                }
                projectService.update(projectPool);
                System.out.println("SUCCESS");
            } else {
                throw new NoPermission();
            }
        }
        catch (NullPointerException e){
            throw new ObjectIsNotFound();
        }
    }

    @Override
    public boolean secureCommand() {
        return true;
    }

    public ProjectUpdateCommand(ServiceLocator bootstrap) {
        setServiceLocator(bootstrap);
    }
}
