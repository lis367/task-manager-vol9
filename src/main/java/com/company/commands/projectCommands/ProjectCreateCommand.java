package com.company.commands.projectCommands;

import com.company.Interfaces.ProjectServiceInterface;
import com.company.Interfaces.ServiceLocator;
import com.company.commands.AbstractCommand;
import com.company.exception.EmptyField;
import com.company.exception.WrongDateFormat;
import com.company.service.TerminalServiceImpl;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Scanner;

@NoArgsConstructor

public final class ProjectCreateCommand extends AbstractCommand {

    @Override
    public String command() {
        return "project-create";
    }

    @Override
    public String description() {
        return "Create new project.";
    }

    @Override
    public void execute() throws Exception {
        @NotNull
        final ProjectServiceInterface projectService = (ProjectServiceInterface) serviceLocator.getService("Project-Service");
        @NotNull
        final TerminalServiceImpl terminalService = (TerminalServiceImpl) serviceLocator.getService("Terminal-service");
        System.out.println("ENTER NAME:");
        @Nullable
        String name = terminalService.nextLine().trim();
        if (name.isEmpty()) {
           throw new EmptyField();
        }
        System.out.println("ENTER THE TIME OF THE BEGINNING OF THE PROJECT");
        @Nullable
        final String dateStart = terminalService.nextLine().trim();

        System.out.println("ENTER THE END TIME OF THE PROJECT");
        @Nullable
        final String dateEnd = terminalService.nextLine().trim();
        if(!serviceLocator.isValidDate(dateStart)|!serviceLocator.isValidDate(dateEnd)){
            throw new WrongDateFormat();
        }
        System.out.println("ENTER THE DESCRIPTION OF THE PROJECT");
        final String description = terminalService.nextLine().trim();
        SimpleDateFormat dateFormatter = new SimpleDateFormat("dd.MM.yyyy");
        System.out.println("YOUR PROJECT ID IS");
        System.out.println(projectService.projectCreate(name, dateFormatter.parse(dateStart),dateFormatter.parse(dateEnd), serviceLocator.getUser().getUserId(),description));


    }
    public boolean secureCommand() {
        return true;
    }

    public ProjectCreateCommand(ServiceLocator bootstrap) {
        setServiceLocator(bootstrap);
    }


}
