package com.company.commands.userCommands;

import com.company.Interfaces.ServiceLocator;
import com.company.Interfaces.UserServiceInterface;
import com.company.commands.AbstractCommand;
import com.company.entity.User;
import com.company.exception.NoPermission;
import com.company.exception.ObjectIsNotFound;
import com.company.service.TerminalServiceImpl;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
@NoArgsConstructor

public final class UserDeleteCommand extends AbstractCommand {

    @Override
    public String command() {
        return "user-delete";
    }

    @Override
    public String description() {
        return "Delete user from repository";
    }

    @Override
    public void execute() throws Exception {
        @NotNull
        final UserServiceInterface userServiceImpl = (UserServiceInterface) serviceLocator.getService("User-Service");
        final TerminalServiceImpl terminalService = (TerminalServiceImpl) serviceLocator.getService("Terminal-Service");


        System.out.println("ENTER LOGIN");
        final String login = terminalService.nextLine();
        System.out.println("ENTER PASSWORD");
        final String password = userServiceImpl.generateMD5(terminalService.nextLine());
        final User user = userServiceImpl.read(login,password);
        try{
        if(user.getUserId().equals(serviceLocator.getUser().getUserId())){
            userServiceImpl.remove(user);
            System.out.println("Profile deleted successfully");
        }
        else{
           throw new NoPermission();
        }}
        catch (NullPointerException e){
            throw new ObjectIsNotFound();
        }
    }

    @Override
    public boolean secureCommand() {
        return true;
    }

    public UserDeleteCommand(ServiceLocator bootstrap) {
        setServiceLocator(bootstrap);
    }
}
