package com.company;

import com.company.commands.AboutCommand;
import com.company.commands.HelpCommand;
import com.company.commands.projectCommands.*;
import com.company.commands.taskCommands.*;
import com.company.commands.userCommands.*;
import com.company.util.Bootstrap;

public class Application {

    private static final Class[] CLASSES = {
            HelpCommand.class, AboutCommand.class, ProjectClearCommand.class, ProjectCreateCommand.class, ProjectListCommand.class,
            ProjectUpdateCommand.class, ProjectRemoveCommand.class,ProjectViewCommand.class, TaskClearCommand.class, TaskCreateCommand.class,
            TaskListCommand.class, TaskRemoveCommand.class, TaskUpdateCommand.class, TaskViewCommand.class, UserAuthorizationCommand.class,
            UserEndSessionCommand.class, UserRegistrationCommand.class, UserUpdateCommand.class,UserUpdatePasswordCommand.class, UserLoadFromDBCommand.class,
            UserViewCommand.class, UserDeleteCommand.class

    };


    public static void main(String[] args) throws Exception {
        Bootstrap bootstrap = new Bootstrap();
        bootstrap.initClasses(CLASSES);
        bootstrap.start();
    }

}
