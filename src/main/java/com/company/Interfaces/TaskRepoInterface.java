package com.company.Interfaces;

import com.company.entity.Task;

import java.util.ArrayList;

public interface TaskRepoInterface {

     void persist(Task task);  //create
     Task findOne(String id);  // read
     ArrayList findAll(String id); // readAll
     void merge (Task task); //update
     void remove(String id); // delete
     void removeAll(); // deleteAll
     ArrayList getTasksFromProject(String id); // Get list of Task from One Project;



}
