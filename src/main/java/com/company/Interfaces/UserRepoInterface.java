package com.company.Interfaces;

import com.company.entity.User;

import java.io.IOException;

public interface UserRepoInterface {

    void persist(User user); //create
    User findOne(String login, String password); //read
    void merge(User user); //update
    void remove(User user); //delete
    void loadFromFile() throws IOException;  // Load Users from File
    void save() throws IOException; // Save new User in File

}
